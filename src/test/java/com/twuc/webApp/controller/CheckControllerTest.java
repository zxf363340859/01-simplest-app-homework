package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
public class CheckControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_check_function() throws Exception {
        mockMvc.perform(post("/api/check").content("{\n" +
                "  \"operandLeft\": \"1\",\n" +
                "  \"operandRight\": \"1\",\n" +
                "  \"operation\": \"+\",\n" +
                "  \"expectedResult\": \"2\"\n" +
                "}\n").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE))
                .andExpect(content().string("{\"correct\":true}"));
    }

    @Test
    void test_advanced_check_function() throws Exception {
        mockMvc.perform(post("/api/check/advanced").content("{\n" +
                "  \"operandLeft\": \"2\",\n" +
                "  \"operandRight\": \"2\",\n" +
                "  \"operation\": \"*\",\n" +
                "  \"expectedResult\": \"5\",\n" +
                "  \"checkType\": \">\"\n" +
                "}\n").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE))
                .andExpect(content().string("{\"correct\":false}"));
    }
}
