package com.twuc.webApp.controller;


import com.twuc.webApp.util.CalculationTableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
public class ChangeableCalculationTableControllerTest {

    @Autowired
    MockMvc mockMvc;


    @Test
    void test_changeable_plus_table() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/tables/plus/changeable").param("start", "3").param("end", "10")).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo(CalculationTableUtil.plusTable(3, 10));
    }

    @Test
    void test_changeable_multiply_table() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/tables/multiply/changeable").param("start", "3").param("end", "10")).andReturn().getResponse().getContentAsString();
        System.out.println(contentAsString);
        assertThat(contentAsString).isEqualTo(CalculationTableUtil.multiplyTable(3, 10));
    }
}
