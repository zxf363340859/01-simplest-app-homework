package com.twuc.webApp.controller;

import com.twuc.webApp.util.AdvancedCalculationTableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class AdvancedChangeableCalculationTableControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void test_advanced_plus_and_return_json() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/tables/plus/advanced/json").param("start", "2").param("end", "4")).andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString, AdvancedCalculationTableUtil.plusTableWithJson(2, 4));
    }

    @Test
    void test_advanced_mutiply_and_return_json() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/tables/mutiply/advanced/json").param("start", "2").param("end", "4")).andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString, AdvancedCalculationTableUtil.mutiplyTableWithJson(2, 4));
    }

    @Test
    void test_advanced_plus_and_return_html() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/tables/plus/advanced/html").param("start", "2").param("end", "4")).andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString, AdvancedCalculationTableUtil.plusTableWithHtml(2, 4));
    }

    @Test
    void test_advanced_mutiply_and_return_html() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/tables/mutiply/advanced/html").param("start", "2").param("end", "4")).andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString, AdvancedCalculationTableUtil.mutiplyTableWithHtml(2, 4));
    }

}
