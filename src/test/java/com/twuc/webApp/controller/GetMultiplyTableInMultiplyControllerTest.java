package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.twuc.webApp.controller.MultiplyController.nineMultiplyNineTable;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class GetMultiplyTableInMultiplyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_return_200_as_response_status() throws Exception {
        int status = mockMvc.perform(get("/api/tables/multiply")).andReturn().getResponse().getStatus();
        assertThat(status).isEqualTo(200);
    }

    @Test
    public void should_return_text_pain_as_content_type() throws Exception {
        String contentType = mockMvc.perform(get("/api/tables/multiply")).andReturn().getResponse().getContentType();
        assertThat(contentType).isEqualTo("text/plain;charset=UTF-8");
    }


}
