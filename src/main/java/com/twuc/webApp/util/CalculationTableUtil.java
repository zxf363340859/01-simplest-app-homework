package com.twuc.webApp.util;

public class CalculationTableUtil {


    public static String plusTable(int start, int end){
        String s = "";
        for (int i = start; i < end; i++) {
            for (int j = 1; j <= i; j++) {
                s += j + "+" + i + "=" + (j + i)+"\t";
            }
            s += "\r\n";
        }
        return s;
    }

    public static String multiplyTable(int start, int end){
        String s = "";
        for (int i = start; i < end; i++) {
            for (int j = 1; j <= i; j++) {
                s += j + "+" + i + "*" + (j * i)+"\t";
            }
            s += "\r\n";
        }
        return s;
    }

}
