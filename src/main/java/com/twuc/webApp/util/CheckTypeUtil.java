package com.twuc.webApp.util;

public class CheckTypeUtil {

    public static boolean judgeExpressionEqualsWithPlus(String operandLeft, String operandRight, String expectedResult, String checkType) {
        if(checkType.equals("=")) {
            return Integer.parseInt(operandLeft)
                    + Integer.parseInt(operandRight)
                    == Integer.parseInt(expectedResult);
        } if(checkType.equals("<")) {
            return Integer.parseInt(operandLeft)
                    + Integer.parseInt(operandRight)
                    < Integer.parseInt(expectedResult);
        }else {
            return Integer.parseInt(operandLeft)
                    + Integer.parseInt(operandRight)
                    > Integer.parseInt(expectedResult);
        }
    }

    public static boolean judgeExpressionEqualsWithMultiply(String operandLeft, String operandRight, String expectedResult, String checkType) {
        if(checkType.equals("=")) {
            return Integer.parseInt(operandLeft)
                    * Integer.parseInt(operandRight)
                    == Integer.parseInt(expectedResult);
        } if(checkType.equals("<")) {
            return Integer.parseInt(operandLeft)
                    * Integer.parseInt(operandRight)
                    < Integer.parseInt(expectedResult);
        }else {
            return Integer.parseInt(operandLeft)
                    * Integer.parseInt(operandRight)
                    > Integer.parseInt(expectedResult);
        }
    }

}
