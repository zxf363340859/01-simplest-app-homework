package com.twuc.webApp.util;

public class AdvancedCalculationTableUtil {

    public static String plusTableWithJson(Integer start, Integer end) {
        String s = "";
        for (int i = start; i < end; i++) {
            s += "[";
            for (int j = 1; j <= i; j++) {
                if(j == i) {
                    s += "\"" + j + "+" + i + "=" + (j + i) + "\"";
                } else {
                    s += "\"" + j + "+" + i + "=" + (j + i)+"\",\t";
                }

            }
            s += "]\r\n";
        }
        return s;
    }

    public static String plusTableWithHtml(Integer start, Integer end) {
        String s = "<ol>";
        for (int i = start; i < end; i++) {
            s += "<li><ol>";
            for (int j = 1; j <= i; j++) {
                    s +=  "<li>" + j + "+" + i + "=" + (j + i) + "</li>";
            }
            s += "</ol></li>";
        }
        s += "</ol>";
        return s;
    }

    public static String mutiplyTableWithJson(Integer start, Integer end) {
        String s = "";
        for (int i = start; i < end; i++) {
            s += "[";
            for (int j = 1; j <= i; j++) {
                if(j == i) {
                    s += "\"" + j + "*" + i + "=" + (j * i) + "\"";
                } else {
                    s += "\"" + j + "*" + i + "=" + (j * i)+"\",\t";
                }

            }
            s += "]\r\n";
        }
        return s;
    }

    public static String mutiplyTableWithHtml(Integer start, Integer end) {
        String s = "<ol>";
        for (int i = start; i < end; i++) {
            s += "<li><ol>";
            for (int j = 1; j <= i; j++) {
                s +=  "<li>" + j + "*" + i + "=" + (j * i) + "</li>";
            }
            s += "</ol></li>";
        }
        s += "</ol>";
        return s;
    }

}
