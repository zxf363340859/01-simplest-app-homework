package com.twuc.webApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlusController {

    @GetMapping("/api/tables/plus")
    public String getPlusTable() {
        return ninePlusNine();
    }

    public static String ninePlusNine(){
        String s = "";
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                s += j + "+" + i + "=" + (j + i)+"\t";
            }
            s += "\r\n";
        }
        return s;
    }

}
