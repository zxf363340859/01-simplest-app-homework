package com.twuc.webApp.controller;

import com.twuc.webApp.util.AdvancedCalculationTableUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdvancedChangeableCalculationTableController {

    @GetMapping("/api/tables/plus/advanced/json")
    public String getPlusTableWithJson(@RequestParam Integer start, @RequestParam Integer end) {
        return AdvancedCalculationTableUtil.plusTableWithJson(start, end);
    }

    @GetMapping("/api/tables/mutiply/advanced/json")
    public String getMutiplyTableWithJson(@RequestParam Integer start, @RequestParam Integer end) {
        return AdvancedCalculationTableUtil.mutiplyTableWithJson(start, end);
    }

    @GetMapping("/api/tables/plus/advanced/html")
    public String getPlusTableWithHtml(@RequestParam Integer start, @RequestParam Integer end) {
        return AdvancedCalculationTableUtil.plusTableWithHtml(start, end);
    }

    @GetMapping("/api/tables/mutiply/advanced/html")
    public String getMutiplyTableWithHtml(@RequestParam Integer start, @RequestParam Integer end) {
        return AdvancedCalculationTableUtil.mutiplyTableWithHtml(start, end);
    }

}
