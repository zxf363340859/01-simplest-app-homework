package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiplyController {

    @GetMapping("/api/tables/multiply")
    public String getMultiplyTable() {
        return nineMultiplyNineTable();
    }

    public static String nineMultiplyNineTable(){
        String s = "";
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                s += j + "*" + i + "=" + (j * i)+"\t";
            }
            s += "\r\n";
        }
        return s;
    }

}
