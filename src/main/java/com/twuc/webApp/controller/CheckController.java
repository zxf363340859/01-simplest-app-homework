package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.bean.AdvancedCheck;
import com.twuc.webApp.bean.Check;
import com.twuc.webApp.bean.CheckResponse;
import com.twuc.webApp.util.CheckTypeUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckController {

    @PostMapping("/api/check")
    public String check(@RequestBody Check check) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        CheckResponse checkResponse = new CheckResponse();
        boolean result = false;
        if(check.getOperation().equals("+")) {
            result = (Integer.parseInt(check.getOperandLeft())
                    + Integer.parseInt(check.getOperandRight()) )
                    == Integer.parseInt(check.getExpectedResult());
        }else {
            result = (Integer.parseInt(check.getOperandLeft())
                    * Integer.parseInt(check.getOperandRight()) )
                    == Integer.parseInt(check.getExpectedResult());
        }
        checkResponse.setCorrect(result);
        return objectMapper.writeValueAsString(checkResponse);
    }

    @PostMapping("/api/check/advanced")
    public String advancedCheck(@RequestBody AdvancedCheck advancedCheck) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        CheckResponse checkResponse = new CheckResponse();
        boolean result = false;
        if(advancedCheck.getOperation().equals("+")) {
            result = CheckTypeUtil.judgeExpressionEqualsWithPlus(advancedCheck.getOperandLeft()
                    ,advancedCheck.getOperandRight()
                    ,advancedCheck.getExpectedResult()
                    ,advancedCheck.getCheckType());
        }else {
            result = CheckTypeUtil.judgeExpressionEqualsWithMultiply(advancedCheck.getOperandLeft()
                    ,advancedCheck.getOperandRight()
                    ,advancedCheck.getExpectedResult()
                    ,advancedCheck.getCheckType());
        }
        checkResponse.setCorrect(result);
        return objectMapper.writeValueAsString(checkResponse);
    }

}
