package com.twuc.webApp.controller;

import com.twuc.webApp.util.CalculationTableUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChangeableCalculationTableController {


    @GetMapping("/api/tables/plus/changeable")
    public String getPlusTable(@RequestParam Integer start, @RequestParam Integer end) {
        return CalculationTableUtil.plusTable(start, end);
    }

    @GetMapping("/api/tables/multiply/changeable")
    public String getMultiplyTable(@RequestParam Integer start, @RequestParam Integer end) {
        return CalculationTableUtil.multiplyTable(start, end);
    }

}
