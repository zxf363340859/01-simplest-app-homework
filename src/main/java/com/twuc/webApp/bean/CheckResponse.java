package com.twuc.webApp.bean;

public class CheckResponse {

    private boolean correct;

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    @Override
    public String toString() {
        return "AdvancedCheckResponse{" +
                "correct=" + correct +
                '}';
    }
}
