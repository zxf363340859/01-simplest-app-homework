package com.twuc.webApp.bean;

public class Check {

    private String operandLeft;
    private String operandRight;
    private String operation;
    private String expectedResult;

    public String getOperandLeft() {
        return operandLeft;
    }

    public void setOperandLeft(String operandLeft) {
        this.operandLeft = operandLeft;
    }

    public String getOperandRight() {
        return operandRight;
    }

    public void setOperandRight(String operandRight) {
        this.operandRight = operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }
}
